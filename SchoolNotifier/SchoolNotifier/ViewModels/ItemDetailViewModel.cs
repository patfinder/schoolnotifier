﻿using System;

using SchoolNotifier.Models;

namespace SchoolNotifier.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Message Message { get; set; }
        public ItemDetailViewModel(Message message = null)
        {
            Title = message?.Name;
            Message = message;
        }
    }
}
