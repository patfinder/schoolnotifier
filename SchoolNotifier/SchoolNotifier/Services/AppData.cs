﻿using System.Collections.Generic;
using System.Text;

namespace SchoolNotifier.Services
{
    public enum AppDataKey
    {
        StudentId = 1,
        LastApiCall,
        LastMessageCount,
    }

    public enum AppMessageKey
    {
        /// <summary>
        /// Background refresh fetch activity
        /// </summary>
        RefreshFetch = 1,
    }
}
