﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Text;
using SchoolNotifier.Models;
using Newtonsoft.Json;

namespace SchoolNotifier.Services
{
    public static class WebServices
    {
        public static string URL_API = "http://smartime.azurewebsites.net/Service.svc/GetMessageOnID?IdString=";

        public static Message[] FetchNewMessages1(string studentId)
        {
            return new Message[] {
                new Message {
                    Id = "Id1",
                    Name = "Message 1"
                },
                new Message {
                    Id = "Id2",
                    Name = "Message 1"
                },
                new Message {
                    Id = "Id3",
                    Name = "Message 1"
                },
            };
        }
        // Gets weather data from the passed URL.
        public static Message[] FetchNewMessages(string studentId)
        {
            // Create an HTTP web request using the URL:
            var request = HttpWebRequest.Create(URL_API + studentId);
            request.ContentType = "application/json";
            request.Method = "GET";

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
                    return new Message[0];
                }

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    var content = reader.ReadToEnd();
                    if (string.IsNullOrWhiteSpace(content))
                    {
                        Console.Out.WriteLine("Response contained empty body...");
                    }
                    else
                    {
                        var apiResult = JsonConvert.DeserializeObject<ApiResult>(content);
                        var messages = JsonConvert.DeserializeObject<Message[]>(apiResult.GetMessageOnIDResult);

                        return messages;
                    }

                    return new Message[0];
                }
            }
        }
    }
}
