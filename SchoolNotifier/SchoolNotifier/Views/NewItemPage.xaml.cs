﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SchoolNotifier.Models;

namespace SchoolNotifier.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewItemPage : ContentPage
    {
        public Message Item { get; set; }

        public NewItemPage()
        {
            InitializeComponent();

            Item = new Message
            {
                Id = "Item name",
                Name = "This is an item description."
            };

            BindingContext = this;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            //MessagingCenter.Send(this, "AddItem", Item);
            //await Navigation.PopModalAsync();
        }
    }
}