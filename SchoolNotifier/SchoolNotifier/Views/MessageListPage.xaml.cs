﻿using SchoolNotifier.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolNotifier.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MessageListPage : ContentPage
	{
        MessagesListViewModel viewModel;

        public MessageListPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new MessagesListViewModel();
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            //var item = args.SelectedItem as SchoolNotifier.Models.Message;
            //if (item == null)
            //    return;

            //await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(item)));

            //// Manually deselect item.
            //MessagesListView.SelectedItem = null;
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new NewItemPage()));
        }

        async void Return_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync(true);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Messages.Count == 0)
                viewModel.LoadMessagesCommand.Execute(null);
        }
    }
}