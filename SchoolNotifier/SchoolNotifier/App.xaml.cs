﻿using System;
using SchoolNotifier.Services;
using SchoolNotifier.ViewModels;
using SchoolNotifier.Views;
using Xamarin.Forms;

namespace SchoolNotifier
{
	public partial class App : Application
	{
        public static string StudentId { get; set; }

		public App ()
		{
			InitializeComponent();

            MainPage = new LoginPage1();
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
            // Handle when your app resumes
            MessagesListViewModel.DoFetchData();
		}
	}
}
