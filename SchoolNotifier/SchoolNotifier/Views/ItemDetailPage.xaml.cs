﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SchoolNotifier.Models;
using SchoolNotifier.ViewModels;

namespace SchoolNotifier.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemDetailPage : ContentPage
	{
        ItemDetailViewModel viewModel;

        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }

        public ItemDetailPage()
        {
            InitializeComponent();

            var message = new Message
            {
                Id = "Item 1",
                Name = "This is an item description."
            };

            viewModel = new ItemDetailViewModel(message);
            BindingContext = viewModel;
        }
    }
}