﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolNotifier.Models
{
    public class Message
    {
        public string Id { get; set; }
        public string Name { get; set; }

        /// <summary>
        ///  Serial number of this item in displayed list.
        /// </summary>
        public int No { get; set; }

        public static bool operator == (Message msg1, Message msg2)
        {
            return msg1.Id == msg2.Id &&
                   msg1.Name == msg2.Name;
        }

        public static bool operator != (Message msg1, Message msg2)
        {
            return !(msg1 == msg2);
        }
    }
}
