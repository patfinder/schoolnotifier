﻿using SchoolNotifier.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SchoolNotifier.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage1 : ContentPage
	{
		public LoginPage1 ()
		{
			InitializeComponent ();
            
            entStudentId.Text = AppDataService.StudentId;
        }

        private async void Login_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(entStudentId.Text))
            {
                DisplayAlert("Lỗi", "Bạn phải nhập mã Học sinh", "Đồng ý");
                return;
            }

            AppDataService.StudentId = entStudentId.Text;

            await Navigation.PushModalAsync(new NavigationPage(new MessageListPage()));
        }

        private async void Reset_Clicked(object sender, EventArgs e)
        {
            AppDataService.StudentId = null;
            entStudentId.Text = null;

            DisplayAlert("Thông báo", "Đã xóa mã Học sinh", "Đồng ý");
        }
    }
}