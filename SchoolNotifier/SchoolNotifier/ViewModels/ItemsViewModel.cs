﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using SchoolNotifier.Models;
using SchoolNotifier.Views;

namespace SchoolNotifier.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        public ObservableCollection<Message> Messages { get; set; }
        public Command LoadItemsCommand { get; set; }

        public ItemsViewModel()
        {
            //Title = "Browse";
            //Messages = new ObservableCollection<Message>();
            //LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            //MessagingCenter.Subscribe<NewItemPage, Message>(this, "AddItem", async (obj, message) =>
            //{
            //    var _item = message as Message;
            //    Messages.Add(_item);
            //    DataStore.AddItem(_item);
            //});
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Messages.Clear();
                var items = DataStore.GetItems(true);
                foreach (var item in items)
                {
                    Messages.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}