﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using SchoolNotifier.Models;
using SchoolNotifier.Views;
using SchoolNotifier.Services;
using System.Linq;
using UIKit;
using Foundation;

namespace SchoolNotifier.ViewModels
{
    public class MessagesListViewModel : BaseViewModel
    {
        public ObservableCollection<Message> Messages { get; set; }

        public Command LoadMessagesCommand { get; set; }

        private static MessagesListViewModel _instance { get; set; }

        public MessagesListViewModel()
        {
            _instance = this;

            Title = "Browse";
            Messages = new ObservableCollection<Message>();
            LoadMessagesCommand = new Command(async () => await ExecuteLoadMessagesCommand());

            // process new messages
            MessagingCenter.Subscribe<SQLiteDataStore, Message[]>(this, AppMessageKey.RefreshFetch.ToString(), async (obj, messages) =>
            {
                HandleLoadMessage(messages);
            });
        }

        static object _handleLoadMessage = new object();

        void HandleLoadMessage(Message[] messages)
        {
            lock(_handleLoadMessage)
            {
                // Nothing new today !
                if(AppDataService.LastApiCall?.Date == DateTime.Now.Date && AppDataService.MessageCount == messages.Count())
                    return;
                
                // First call today!
                if(AppDataService.LastApiCall?.Date == DateTime.Now.Date)
                {
                    AppDataService.UpdateMessageCount(DateTime.Now, messages.Count());
                }

                // Get only new messages
                var newMessages = messages.Skip(AppDataService.MessageCount ?? 0).ToArray();

                // Save new message to store
                if (newMessages.Any())
                {
                    DataStore.AddItems(newMessages);
                    //Show notification
                    var lastMessage = newMessages.Last();
                    ShowNotification($"{lastMessage.Id} - {lastMessage.Name}");
                }

                // Update to UI
                Messages.Clear();

                //messages = DataStore.GetItems(true).ToArray();
                var count = 0;
                foreach (var message in messages)
                {
                    message.No = ++count;
                    Messages.Add(message);
                }
            }
        }

        private void ShowNotification(string message)
        {
            // create the notification
            var notification = new UILocalNotification();

            // set the fire date (the date time in which it will fire)
            notification.FireDate = NSDate.FromTimeIntervalSinceNow(0);

            // configure the alert
            notification.AlertAction = "SmartTime";
            notification.AlertBody = message;

            // modify the badge
            notification.ApplicationIconBadgeNumber = 1;

            // set the sound to be the default sound
            notification.SoundName = UILocalNotification.DefaultSoundName;

            // schedule it
            UIApplication.SharedApplication.ScheduleLocalNotification(notification);
        }

        async Task ExecuteLoadMessagesCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                // Load data
                var messages = WebServices.FetchNewMessages(AppDataService.StudentId);

                // test only
                //var messages = new Message[0];

                HandleLoadMessage(messages);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public static void DoFetchData()
        {
            if (_instance != null && _instance.IsBusy)
                return;

            if (_instance != null)
                _instance.IsBusy = true;

            try
            {
                // Check for new data, and display it
                if (!string.IsNullOrEmpty(AppDataService.StudentId))
                {
                    var messages = WebServices.FetchNewMessages(AppDataService.StudentId);

                    MessagingCenter.Send<SQLiteDataStore, Message[]>(null, AppMessageKey.RefreshFetch.ToString(), messages);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                if (_instance != null)
                    _instance.IsBusy = false;
            }
        }
    }
}
