﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchoolNotifier.Services
{
    public interface IDataStore<T>
    {
        bool AddItem(T item);
        bool AddItems(T[] items);
        bool UpdateItem(T item);
        bool DeleteItem(T item);
        T GetItem(string id);
        IEnumerable<T> GetItems(bool forceRefresh = false);
    }
}
