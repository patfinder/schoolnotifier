﻿using System;
using Xamarin.Forms;

namespace SchoolNotifier.Services
{
    public static class AppDataService
    {
        public static string StudentId
        {
            get
            {
                if (Application.Current.Properties.Keys.Contains(AppDataKey.StudentId.ToString()))
                {
                    return (string)Application.Current.Properties[AppDataKey.StudentId.ToString()];
                }

                return null;
            }
            set
            {
                Application.Current.Properties[AppDataKey.StudentId.ToString()] = value;
            }
        }

        public static DateTime? LastApiCall
        {
            get
            {
                if (Application.Current.Properties.Keys.Contains(AppDataKey.LastApiCall.ToString()))
                {
                    return (DateTime)Application.Current.Properties[AppDataKey.LastApiCall.ToString()];
                }

                return null;
            }
            private set
            {
                Application.Current.Properties[AppDataKey.LastApiCall.ToString()] = value;
            }
        }

        /// <summary>
        /// Message count of most recent API call.
        /// </summary>
        public static int? MessageCount
        {
            get
            {
                if (Application.Current.Properties.Keys.Contains(AppDataKey.LastMessageCount.ToString()))
                {
                    return (int)Application.Current.Properties[AppDataKey.LastMessageCount.ToString()];
                }

                return null;
            }
            private set
            {
                Application.Current.Properties[AppDataKey.LastMessageCount.ToString()] = value;
            }
        }

        private static readonly object _updateMessageCountLock = new object();

        public static void UpdateMessageCount(DateTime fetchTime, int messageCount)
        {
            lock(_updateMessageCountLock)
            {
                MessageCount = messageCount;
                LastApiCall = fetchTime;
            }
        }
    }
}
