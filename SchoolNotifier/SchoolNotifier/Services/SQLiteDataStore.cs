﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using SchoolNotifier.Models;
using SQLite;

[assembly: Xamarin.Forms.Dependency(typeof(SchoolNotifier.Services.SQLiteDataStore))]
namespace SchoolNotifier.Services
{
    public class SQLiteDataStore : IDataStore<Message>
    {
        private List<Message> messages;
        private SQLiteConnection Db { get; set; }

        public SQLiteDataStore()
        {
            // Automatically create db
            Db = new SQLiteConnection(DbPath);

            Db.CreateTable<Message>();
        }

        private static SQLiteDataStore _instance;
        public static SQLiteDataStore Instance
        {
            get
            {
                if(_instance ==  null)
                {
                    _instance = new SQLiteDataStore();
                }
                return _instance;
            }
        }

        private string DbName => "SchoolDb.sqlite";

        private string DbPath => new FileHelper().GetLocalFilePath(DbName);

        public bool AddItem(Message message)
        {
            return Db.Insert(message) >= 0;
        }

        public bool AddItems(Message[] messages)
        {
            messages.ToList().ForEach(message => Db.Insert(message));

            return true;
        }

        public bool UpdateItem(Message message)
        {
            throw new Exception("Not implemented");
        }

        public bool DeleteItem(Message message)
        {
            throw new Exception("Not implemented");
        }

        public Message GetItem(string id)
        {
            return Db.Get<Message>(id);
        }

        public IEnumerable<Message> GetItems(bool forceRefresh = false)
        {
            return Db.Table<Message>();
        }
    }
}